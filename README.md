# Chrome Extension Template

Template for newbie creating new chrome extension

## Features

  * Manage using widget
  * Auto generating for enable/disable settings in option page
  * Including up-to-date libraries jquery, lodash, momentjs, async
  * Support helpful utilities functions such as wait_for, $.wait...
  * Support locales for multiple languages
  * ...updating

## How it works
  * All widgets will be loaded and widget's `initilize` will be executed on `document_start` from [content_scripts](https://developer.chrome.com/extensions/content_scripts)
  * After document has finished loading (document.readyState is [interactive](https://developer.mozilla.org/en/docs/web/api/document/readystate) or [complete](https://developer.mozilla.org/en/docs/web/api/document/readystate)), widget's `start` will be executed.

  ![Widget's Workflow](http://i.imgur.com/637kkKI.jpg)

## Identification

  * `widgets` is all widgets will be injected to page that matches in content_script
  * `widget` is one of `widgets`
  * `matches` is an API of `widget` for filtering
  * `running list` is a filtered list of `widgets` will be run on the page.
  * `initilize` is a function of `widget` that do some preparations - is executed when dom is loading
  * `start` is a function of `widget` is executed after dom is ready

## Widget's APIs

All APIs are optional and depend on how you want the `widget` run.

### name (string)

`widget`'s name, just for identify what's it

### matches (string)

If existed, `widget` will be checked for matching with `location.href`. And if not match, `widget` will be removed from `running list` on that page. 

Otherwise `widget` will be added to `running list`.

### initilize (function)

This function will be run after `widgets` are filtered to `running list`.

### start (function)

This function will be run after dom finished loading.

### on_dom_changed (function)

This function will be run when dom changed (removed or inserted)

### on_settings_changed (function)

This function will be run when settings changed
