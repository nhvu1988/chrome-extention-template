(function() {

	'use strict';

	var widget = {
		
		name: 'New Widget',

		/* [optional] */
		// matches: 'google.com',

		/* 
		 * [optional] - This function will run after filtered
		 */
		initilize: function () {
			console.log('go to initilize');
		},

		/* 
		 * [optional] - This function will run when readyState is interactive or complete
		 * readyState: https://developer.mozilla.org/en/docs/web/api/document/readystate
		 */
		start: function () {
			console.log('go to start');
			// console.log($('title').text());
		},

		on_url_changed: function () {
			console.log('go to on_url_changed');
		},

		/* 
		 * [optional] - Listen the settings changing
		*/
		on_settings_changed: function (changes) {
			console.log('go to on_settings_changed');
		}
	}

	widgets.push(widget);

})();
