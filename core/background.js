(function () {
	'use strict';

	chrome.tabs.onUpdated.addListener(
		function (tabId, changeInfo, tab) {
			if (changeInfo.url) {
				chrome.tabs.sendMessage(tabId, {event: 'tabs.onUpdated.url'}, function(response) {});
			}
		}
	);

})();
