var util;

(function () {
	'use strict';

	util = {

		wait_for: function (test, callback, timer, timeout) {
			let i, t,
				done = function (err) {
					clearInterval(i);
					clearTimeout(t);
					callback && callback(err);
				};

			// run timer for checking test fn
			i = setInterval(function () {
				if (test && test()) {
					done();
				}
			}, timer || 500);

			// set timeout for checking the timeout is over
			t = setTimeout(function () {
				done(new Error('Timeout'));
			}, timeout || 30000);
		},

		retrieve_window_variables: function (variables) {
			var script = $('<script id="temp_script"></script>'),
                content = '',
				variable, i;

			for (i in variables) {
				variable = variables[i];

				content += 'if (typeof ' + variable + ' !== "undefined"){' +
					'document.body.setAttribute("tmp_' + variable + '", ' + variable + ');}';
			   
			}

			script.text(content).appendTo(document.body || document.head || document.documentElement);

			for (i in variables) {
				variable = variables[i];
				variables[i] = document.body.getAttribute('tmp_' + variable);
				document.body.removeAttribute('tmp_' + variable);
			}

			script.remove();

			return variables;
		},

        parse_qs: function (url) {
            var obj = {};

            url = url || location.search;

            url.slice(url.indexOf('?') + 1)
                .split('&')
                .forEach(function (a) {
                    if (a) {
                        a = a.split('=');
                        obj[a[0]] = decodeURIComponent(a.slice(1).join('='));
                    }
                });

            return obj;
        },

        json_parse: function (string) {
            try { return JSON.parse(string); }
            catch (e) { return null; }
        }
	};

	// wait for element util the timeout is over
	// - selector is css selector supported by $ function
	// - interval timer defaults to 500ms
	// - timeout defaults to 30s
	// - callback will receive the first param as error if any
	//   and second param contains found element(s)
	$.wait = (selector, callback, timer, timeout) => {
		var timeout = +new Date() + (timeout || 30000),
			i = setInterval(function () {

				var element = $(selector);

				if (element.length || (+new Date() >= timeout)) {
					clearInterval(i);
					callback && callback(
						element.length ? null : new Error('TIMEOUT: Element not found'),
						element
					);
				}
			}, timer || 500);
	};
})();
