var settings = null,
	widgets = [];

(function () {
	'use strict';

	function start () {
		var storage = chrome.storage.sync,
			_settings = settings || {};

		_settings.set = function (key, value) {
			_settings[key] = value;
			_settings.save();
		};

		_settings.del = function (key) {
			_settings[key] = null;
			_settings.save();
		}

		_settings.save = function (callback) {
			storage.set(JSON.parse(JSON.stringify(_settings)), function () {
				if (chrome.runtime.lastError) {
					return callback && callback(chrome.runtime.lastError);
				}

				callback && callback();
			});
		};

		_settings.refresh = function (callback) {
			storage.get(null, function (items) {
				function parse_setting (set) {
					var key = set[0],
						type = set[1].type,
						value = set[1].value,
						children;

					switch (type) {
						case 'boolean':
							if (!_.isBoolean(_settings[key])) {
								_settings[key] = value || false;
							}
							break;

						case 'string':
						case 'number':
						case 'color':
							if (!_.isUndefined(_settings[key])) {
								_settings[key] = value || null;
							}
							break;
					}

					if (set.length > 2) {
						set = _(set).drop(2);
						_(set).forEach(parse_setting);
					}
				};

				_(config.settings).forEach(parse_setting);

				callback && callback();
			});
		};

		_settings.refresh();

		settings = _settings;
	};

	start();

})();
