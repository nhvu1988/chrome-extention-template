(function () {
	'use strict';

	Vue.component('settings', {
		props: ['settings'],
		template: ` 
			<ul>
				<template v-for="(key, option) in settings">
					<li>
						<setting :key="key" :type="option[0]" 
							:value="!~['undefined', 'object'].indexOf(typeof option[1]) ? option[1] : null"
							:children="~[typeof option[1], typeof option[2]].indexOf('object') ? (option[2] || option[1]) : null"
						>
						</setting>
					</li>
				</template>
			</ul>
		`
	});

	Vue.component('setting', {
		props: ['key', 'type', 'value', 'children'],
		template: ` 
			<template v-if="type == 'label'">
				<setting-label :label="key"></setting-label>
			</template>
			<template v-if="type == 'boolean'">
				<setting-boolean :label="key" :value="value"></setting-label>
			</template>
			<template v-if="type == 'string'">
				<setting-string :label="key" :value="value"></setting-label>
			</template>
			<template v-if="type == 'number'">
				<setting-number :label="key" :value="value"></setting-label>
			</template>
			<template v-if="type == 'color'">
				<setting-color :label="key" :value="value"></setting-label>
			</template>
			<template v-if="children">
				<settings :settings="children"></settings>
			</template>
		`
	});

	Vue.component('setting-label', {
		props: ['label'],
		template: `<label>{{ label }}</label>`
	});

	Vue.component('setting-boolean', {
		props: ['label', 'value'],
		template: `<label><input type="checkbox"/> {{ label }}</label>`
	});

	Vue.component('setting-string', {
		props: ['label', 'value'],
		template: `<label>{{ label }}</label>
		<input type="text"/>
		`
	});

	Vue.component('setting-number', {
		props: ['label'],
		template: '<label>{{ label }}</label>'
	});

	Vue.component('setting-color', {
		props: ['label'],
		template: '<label>{{ label }}</label>'
	});

	new Vue({
		el: '#settings',
		data: { settings: config.settings }
	});

})();
