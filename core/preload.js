(function () {
	'use strict';

	var _widgets = [],
		event_registered = false;

	function start () {
		filter_widgets();
	}

	function filter_widgets () {
		// filter widget by matches
		_widgets = _(widgets)
			.filter(function (widget) {
				return !(widget.matches && !location.href.match(widget.matches));
			})
			.value();

		_widgets.all = function () {
			var len = this.length,
				fn = arguments[0];

			while (len--) {
				if (widgets[len][fn]) {
					widgets[len][fn].apply(widgets[len], arguments[1]);
				}
			}
		}

		// and initilize widgets
		_widgets.all('initilize');

		wait_for_dom_loaded();
	}

	function wait_for_dom_loaded () {
		util.wait_for(
			function () {
				return (document.readyState === 'interactive' || document.readyState === 'complete');
			},
			function () {
				_widgets.all('start');
				register_events();
			}
		);
	}

	// register events
	function register_events () {
		if (event_registered) return;

		event_registered = true;

		// listen for url changing
		chrome.runtime.onMessage.addListener(
			function (request, sender, sendResponse) {
				switch (request.event) {
					case 'tabs.onUpdated.url':
						// execute on_url_changed event
						_widgets.all('on_url_changed');

						// re-filter widgets
						filter_widgets();
						break;
				}
			}
		);

		// listen for settings changing
		chrome.storage.onChanged.addListener(function (changes) {
			settings.refresh(function () {
				widgets.all('on_settings_changed', [changes]);
			});
		});
	}

	start();
})();