var config;

(function () {
	'use strict';

	config = {
		/*
		 * settings is a list of settings, each setting is an object contains up to 3 items
		 * - first item is setting type, can be 'boolean', 'string', 'number', 'color' or 'label'
		 * - second item contains default value
		 *		+ value is optional, default to false if type is 'boolean', null for the others
		 * - the third is list children settings if existed
		 */
		settings: {
			google_global: ['label', {
				get_title: ['boolean', true],
				get_description: ['boolean', true],
				get_search: ['boolean', true]
			}],
			google_vn: ['boolean', {
				show_darkmode: ['boolean'],
				show_search: ['boolean'],
				show_comment: ['boolean', true, {
					my_comment: ['boolean'],
					other_comment: ['boolean']
				}]
			}]
		}
	};

})();
